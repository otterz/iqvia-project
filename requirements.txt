Flask==1.1.1
SQLAlchemy==1.3.11
pytest==5.2.2
celery==4.4.0rc4
celery[redis]
mimesis==3.3.0
