import functools

from mimesis import Person
from contactsapp import create_app
from contactsapp.persistence import get_persistence
from contactsapp.db import contacts
from contactsapp.celery import make_celery
from datetime import datetime, timedelta, timezone

app = make_celery(create_app())


def generate_contact():
    fake = Person('en')
    return {
        'username': fake.identifier(),
        'firstname': fake.name(),
        'lastname': fake.last_name(),
        'emails': [
            {'address': fake.email()},
            {'address': fake.email()},
            {'address': fake.email()},
        ]
    }


def flask_context(func):
    
    @functools.wraps(func)
    def do_in_flask_context(*args, **kwargs):
        flask_app = create_app()
        with flask_app.app_context():
            func(*args, **kwargs)

    return do_in_flask_context
    

@app.task
@flask_context
def create_new_contact():
    store = get_persistence(contacts())
    store.create_contact(generate_contact())

        
@app.task
@flask_context
def delete_old_contacts():
    store = get_persistence(contacts())
    minute_ago = datetime.now(timezone.utc) - timedelta(minutes=1)
    store.delete_contacts(minute_ago)
        
    
app.conf.beat_schedule = {
    "create-contacts-task-15": {
        "task": "contactsapp.tasks.create_new_contact",
        "schedule": 15.0
    },
    "delete-old-contacts-task-60": {
        "task": "contactsapp.tasks.delete_old_contacts",
        "schedule": 60.0
    }
}

app.conf.timezone = 'UTC'
app.conf.broker_transport_options = {'visibility_timeout': 36000000}
