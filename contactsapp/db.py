"""
This module contains SQLAlchemy-related code: ORM, session creation 
and flask database initialization commands.
"""

from sqlalchemy import Column, Integer, String, ForeignKey, DateTime
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship, validates
from sqlalchemy.sql import func

from flask.cli import with_appcontext
from flask import current_app
import click

Base = declarative_base()


class Contact(Base):
    __tablename__ = 'contact'
    id = Column(Integer, primary_key=True)

    username = Column(String(50), nullable=False, unique=True)
    firstname = Column(String(50), nullable=False)
    lastname = Column(String(50), nullable=False)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    
    emails = relationship('Email', lazy="joined")
    

class Email(Base):
    __tablename__ = 'email'
    id = Column(Integer, primary_key=True)

    address = Column(String(50), nullable=False)
    contact_id = Column(Integer, ForeignKey('contact.id'))
    contact = relationship('Contact', back_populates="emails")

    @validates('address')
    def validate_address(self, key, address):
        assert '@' in address
        return address
    
    
class ContactsSessionMaker():
    """ Abstracts session creation (engine creation and session
    maker). """
    
    URI = 'sqlite:///'
    
    def __init__(self, path=':memory:'):
        self.engine = create_engine(ContactsSessionMaker.URI + path)
        
    def make(self):
        session_maker = sessionmaker(bind=self.engine)
        return session_maker()


def init_db(db_path):
    maker = ContactsSessionMaker(db_path)
    Base.metadata.create_all(maker.engine)


@click.command('init-db')
@with_appcontext
def init_db_command():
    dbpath = current_app.config['DATABASE']
    init_db(dbpath)
    

def init_app(app):
    app.cli.add_command(init_db_command)


def contacts():
    dbpath = current_app.config['DATABASE']
    return ContactsSessionMaker(dbpath)
    
