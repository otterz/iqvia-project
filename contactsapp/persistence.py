"""
This module contains implementations of the persistence duck-type interface.

Each class can be a concrete implementation of a persistence type, by setting
and implementing instance members (get_all_contacts, get_contact_by_username,
 etc.) This makes having multiple different implementations possible. For 
example there can be an SQLAlchemy implementation (such as 
SQLAlchemyPersistence), a NoSQL implementation and so on.
"""
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import IntegrityError as SQLAlchemyIntegrityError
from contactsapp.db import Contact, Email


class SQLAlchemyPersistence():

    def __init__(self, session_maker):
        self._session_maker = session_maker

    def session(self):
        return self._session_maker.make()
        
    def get_all_contacts(self):
        return self.session().query(Contact).all()

    def get_contact_by_id(self, id):
        return self.session().query(Contact).\
            filter(Contact.id == id).one_or_none()
    
    def get_contact_by_username(self, name):
        session = self.session()
        try:
            return session.query(Contact).\
                filter(Contact.username == name).one()
        except NoResultFound as e:
            raise ItemNotFoundError(e)
        finally:
            if session:
                session.close()

    def create_contact(self, contact_info):
        session = self.session()

        processed_info = SQLAlchemyPersistence._process_dict_info(contact_info)
        contact = Contact(**processed_info)
        try:
            session.add(contact)
            session.commit()
        except SQLAlchemyIntegrityError as e:
            raise IntegrityError(e)

    def update_contact(self, contact_id, updated_info):
        session = self.session()

        session.query(Contact).\
            filter(Contact.id == contact_id).\
            update(SQLAlchemyPersistence._process_dict_info(updated_info))

        session.commit()

        return session.query(Contact).\
            filter(Contact.id == contact_id).one()
        
    def delete_contact(self, contact_id):
        session = self.session()
        contact = session.query(Contact).\
            filter(Contact.id == contact_id).\
            one_or_none()

        if contact:
            session.delete(contact)
            session.commit()

    def get_emails_for_contact(self, contact_id):
        return self.session().query(Email).\
            filter(Email.contact_id == contact_id).all()

    def add_contact_email(self, contact_id, address):
        session = self.session()
        session.add(Email(address=address, contact_id=contact_id))
        session.commit()

    def remove_contact_email(self, email_id):
        session = self.session()
        email = session.query(Email).\
            filter(Email.id == email_id).one_or_none()
        if email:
            session.delete(email)
            session.commit()

    def delete_contacts(self, until):
        session = self.session()
        session.query(Contact).\
            filter(Contact.created_at < until).delete()
        session.commit()
            
    def _process_dict_info(info):
        processed_info = dict(info)
        contact_emails = info.get('emails')
        if contact_emails:
            emails = [Email(address=item['address']) for item in contact_emails]
            processed_info['emails'] = emails

        return processed_info
    

def get_persistence(session_maker):
    """ Returns a default persistence implementation. """
    return SQLAlchemyPersistence(session_maker)


class ItemNotFoundError(Exception):
    pass


class IntegrityError(Exception):
    pass
