import dateutil.parser

from flask import Blueprint, request
from contactsapp.db import contacts
from contactsapp.persistence import get_persistence
from contactsapp.persistence import ItemNotFoundError, IntegrityError
from dateutil.parser import ParserError

blueprint = Blueprint('contacts', __name__, url_prefix='/contacts')


def to_dict(contact):
    return {
        'id': contact.id,
        'username': contact.username,
        'firstname': contact.firstname,
        'lastname': contact.lastname,
        'created_at': contact.created_at,
        'emails': [{
            'id': email.id,
            'address': email.address
        } for email in contact.emails]
    }


@blueprint.route('', methods=('GET',))
def get_contacts():
    store = get_persistence(contacts())
    username_param = request.args.get('username')

    if not username_param:
        all_contacts = store.get_all_contacts()
        return {
            'contacts': [to_dict(contact) for contact in all_contacts]
        }, 200

    try:
        username = store.get_contact_by_username(username_param)
        return to_dict(username), 200
    except ItemNotFoundError:
        return {'status': 'Item not found'}, 404


@blueprint.route('', methods=('POST',))
def create_contact():
    req_json = request.get_json()
    username = req_json.get('username')
    firstname = req_json.get('firstname')
    lastname = req_json.get('lastname')

    if not all([username, firstname, lastname]):
        return {'status': 'All attributes are required'}, 422

    store = get_persistence(contacts())

    try:
        store.create_contact(req_json)
        return {'status': 'success'}, 201
    except IntegrityError:
        return {'status': 'Username already exists'}, 422
    

@blueprint.route('/<int:contact_id>', methods=('PUT',))
def update_contact(contact_id):
    req_json = request.get_json()

    attrs = {}
    for attr in ['username', 'firstname', 'lastname']:
        if req_json.get(attr):
            attrs[attr] = req_json.get(attr)

    store = get_persistence(contacts())
    updated = store.update_contact(contact_id, attrs)
    return to_dict(updated), 200


@blueprint.route('/<int:contact_id>', methods=('DELETE',))
def delete_contact(contact_id):
    store = get_persistence(contacts())
    if store.get_contact_by_id(contact_id):
        store.delete_contact(contact_id)
        return {'status': 'success'}, 200

    return {'status': 'A user with that id does not exist'}, 404


@blueprint.route('/<int:contact_id>/emails', methods=('PUT',))
def add_contact_email(contact_id):
    req_json = request.get_json()
    address = req_json.get('address')

    if not address:
        return {'status': 'Invalid request payload'}, 422
    
    store = get_persistence(contacts())
    try:
        store.add_contact_email(contact_id, address)
        return {
            'status': 'email address added to contact id {}'.format(contact_id)
        }, 200
    except AssertionError:
        return {'status': 'Invalid email address'}, 422


@blueprint.route('/emails/<int:email_id>', methods=('DELETE',))
def remove_email(email_id):
    get_persistence(contacts()).remove_contact_email(email_id)
    return {'status': 'deleted email {}'.format(email_id)}, 200
    

@blueprint.route('', methods=('DELETE',))
def delete_contacts_until():
    store = get_persistence(contacts())
    until_param = request.args.get('until')

    if until_param:
        until_datetime = None

        try:
            until_datetime = dateutil.parser.parse(until_param)
        except ParserError:
            return {'status': 'Invalid datetime format. It should be ISO8601'}, 422
        
        store.delete_contacts(until_datetime)

        return {
            'status': 'deleted all contacts older than {}'.format(until_param)
        }, 200

    return {'status': 'no until param was provided'}, 400
