## Running the tests

I used `virtualenv` to create isolated python environment in a venv subdirectory (incl. in .gitignore).

```
virtualenv venv
source venv/bin/activate
```

To install the requirements run:

```
pip3 install -r requirements.txt
```

from the project root.

Install the project as a package to run the tests:

```
pip3 install -e .
```

This will install a `contactsapp` package. After that set the following environment variables:

```
export FLASK_APP=contactsapp
export FLASK_ENV=development
```

Use `pytest -v` to run the tests. If you used `virtualenv` to install pytest you can also run the tests with:

```
python3 -m pytest -v
```

### Running the celery tasks

Celery is configured to work with redis on localhost:6379. Make sure that redis is running on that port. To start the celery tasks run:

```
celery -A contactsapp.tasks worker --loglevel=info --beat --purge -n 1
```

This will start a single worker, purge old tasks and it will start a celery beat periodic task scheduler.


### Running the flask application locally

Export the environment variables as described in the "Running the tests" section. Run `flask init-db`. This will create an sqlite database file in the "instance" subdirectory. Run the app with `flask run`.

### An example session with curl

To create a contact do POST on /contacts:

```
curl -X POST 127.0.0.1:5000/contacts -H 'Content-Type: application/json' -d '{"username": "user1", "firstname": "User1", "lastname": "lastname1"}'
```

This should return HTTP response with status code 200 and a json payload "{'status': 'success'}". Running the same curl command will result in response with status code 422 (unprocessable entry) because the username has the "unique" integrity constraint.


To get the contact details of all contacts GET on /contacts:

```
curl -X GET 127.0.0.1:5000/contacts -H 'Content-Type: application/json'
```

The response should look like this:

```
{
  "contacts": [
    {
      "created_at": "Thu, 14 Nov 2019 07:15:11 GMT", 
      "emails": [], 
      "firstname": "User1", 
      "id": 1, 
      "lastname": "lastname1", 
      "username": "user1"
    }
  ]
}
```

To update contact details do PUT on a specific contact resource. For example, to change the first name and the last name of the contact we just created:

```
curl -X PUT 127.0.0.1:5000/contacts/1 -H 'Content-Type: application/json' -d '{"firstname": "Foo", "lastname": "Bar"}'
```

The response is:

```
{
  "created_at": "Thu, 14 Nov 2019 07:15:11 GMT", 
  "emails": [], 
  "firstname": "Foo", 
  "id": 1, 
  "lastname": "Bar", 
  "username": "user1"
}

```

To add an email to a contact do PUT on email on a specific contact resource. For example, to add email to the contact with id 1 do PUT on /contacts/1/emails:

```
curl -X PUT 127.0.0.1:5000/contacts/1/emails -H 'Content-Type: application/json' -d '{"address": "test@test.org"}'
```

Then the emails will be visible in the emails property fot that contact: 

```
curl -X GET 127.0.0.1:5000/contacts -H 'Content-Type: application/json'
{
  "contacts": [
    {
      "created_at": "Thu, 14 Nov 2019 07:15:11 GMT", 
      "emails": [
        {
          "address": "test2@test.org", 
          "id": 2
        }, 
        {
          "address": "test@test.org", 
          "id": 1
        }
      ], 
      "firstname": "Foo", 
      "id": 1, 
      "lastname": "Bar", 
      "username": "user1"
    }
  ]
}
```

The email addresses can be deleted:

```
curl -X DELETE 127.0.0.1:5000/contacts/emails/1 -H 'Content-Type: application/json'
```

The contacts can be deleted as well:


```
curl -X DELETE 127.0.0.1:5000/contacts/1 -H 'Content-Type: application/json'
```
