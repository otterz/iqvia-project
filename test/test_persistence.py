import pytest

from datetime import datetime, timezone
from contactsapp.db import Contact, Email, ContactsSessionMaker
from contactsapp.persistence import get_persistence
from contactsapp.persistence import IntegrityError, ItemNotFoundError


@pytest.fixture
def store(app, dbpath):
    email_1 = Email(address="foo@bar.com")
    email_2 = Email(address="foo2@bar.com")
    contact_1 = Contact(username="johndoe123",
                        firstname="John",
                        lastname="Doe",
                        created_at=datetime(2019, 11, 13, tzinfo=timezone.utc),
                        emails=[email_1, email_2])

    email_3 = Email(address="bar@baz.com")
    contact_2 = Contact(username="janedoe345",
                        firstname="Jane",
                        lastname="Doe",
                        created_at=datetime(2019, 10, 10, tzinfo=timezone.utc),
                        emails=[email_3])

    session_maker = ContactsSessionMaker(dbpath)
    session = session_maker.make()

    session.add_all([contact_1, contact_2])
    session.commit()
    session.close()
    
    return get_persistence(session_maker)


def test_get_all_contacts(store):
    all_contacts = store.get_all_contacts()
    assert len(all_contacts) == 2


def test_get_contact_by_username(store):
    contact = store.get_contact_by_username("johndoe123")

    assert contact.firstname == "John"
    assert contact.lastname == "Doe"
    assert len(contact.emails) == 2

    
def test_contact_by_id(store):
    assert store.get_contact_by_id(1) is not None
    assert store.get_contact_by_id(1234) is None

    assert len(store.get_contact_by_id(2).emails) == 1

    
def test_get_nonexisting_username_throws_exception(store):
    with pytest.raises(ItemNotFoundError):
        store.get_contact_by_username("nonexisting1234")
        

def test_create_contact(store):
    assert len(store.get_all_contacts()) == 2
    store.create_contact({'username': 'foo',
                          'firstname': 'New',
                          'lastname': 'User'})

    assert len(store.get_all_contacts()) == 3


def test_create_contact_with_emails(store):
    assert len(store.get_all_contacts()) == 2
    store.create_contact({'username': 'foo',
                          'firstname': 'New',
                          'lastname': 'User',
                          'emails': [
                              {'address': 'test@test.com'},
                              {'address': 'test2@example.com'}
                          ]})

    assert len(store.get_all_contacts()) == 3
    
    
def test_create_contact_with_existing_username_raises_error(store):
    with pytest.raises(IntegrityError):
        store.create_contact({'username': 'johndoe123',
                              'firstname': 'New',
                              'lastname': 'User'})

    
def test_update_contact(store):
    updated_contact = store.update_contact(1, {'firstname': 'Jack',
                                               'lastname': 'Bar'})
    assert updated_contact.firstname == "Jack"
    assert updated_contact.lastname == "Bar"

    
def test_delete_contact(store):
    store.delete_contact(1)
    assert len(store.get_all_contacts()) == 1

    # Deleting a contact cascade deletes its associated emails
    assert len(store.get_emails_for_contact(1)) == 0

    
def test_delete_all_contacts_older_than_datetime(store):
    store.delete_contacts(datetime(2019, 11, 1, tzinfo=timezone.utc))
    assert len(store.get_all_contacts()) == 1

    
def test_add_contact_email(store):
    store.add_contact_email(1, 'test3@abc.org')
    assert len(store.get_contact_by_id(1).emails) == 3


def test_remove_contact_email(store):
    store.remove_contact_email(1)
    assert len(store.get_contact_by_id(1).emails) == 1
