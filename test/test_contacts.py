import json
import pytest

from datetime import datetime, timezone
from contactsapp.db import Contact, Email, ContactsSessionMaker


@pytest.fixture
def test_app(app, dbpath):
    """ App fixture with some initial test data """

    email_1 = Email(address="foo@bar.com")
    email_2 = Email(address="foo2@bar.com")
    email_3 = Email(address="bar@baz.com")

    contact_1 = Contact(username="johndoe123",
                        firstname="John",
                        lastname="Doe",
                        created_at=datetime(2019, 11, 13, tzinfo=timezone.utc),
                        emails=[email_1, email_2])

    contact_2 = Contact(username="janedoe345",
                        firstname="Jane",
                        lastname="Doe",
                        created_at=datetime(2019, 11, 12, tzinfo=timezone.utc),
                        emails=[email_3])

    session_maker = ContactsSessionMaker(dbpath)
    session = session_maker.make()

    session.add_all([contact_1, contact_2])
    session.commit()
    session.close()


def test_list_all_contacts(test_app, client):
    response = client.get('/contacts')
    assert response.status_code == 200
    assert json.loads(response.data.decode('utf-8')) == {
        'contacts': [
            {
                'id': 1,
                'username': 'johndoe123',
                'firstname': 'John',
                'lastname': 'Doe',
                'created_at': 'Wed, 13 Nov 2019 00:00:00 GMT',
                'emails': [
                    {
                        'id': 2,
                        'address': 'foo2@bar.com'
                    }, {
                        'id': 1,
                        'address': 'foo@bar.com'
                    }
                ]
            }, {
                'id': 2,
                'username': 'janedoe345',
                'firstname': 'Jane',
                'lastname': 'Doe',
                'created_at': 'Tue, 12 Nov 2019 00:00:00 GMT',
                'emails': [
                    {
                        'id': 3,
                        'address': 'bar@baz.com'
                    }
                ]
            }]
    }
    

def test_get_contact_by_username(test_app, client):
    response = client.get('/contacts?username=johndoe123')
    assert response.status_code == 200
    assert json.loads(response.data.decode('utf-8')) == {
        'id': 1,
        'username': 'johndoe123',
        'firstname': 'John',
        'lastname': 'Doe',
        'created_at': 'Wed, 13 Nov 2019 00:00:00 GMT',
        'emails': [
            {
                'id': 1,
                'address': 'foo@bar.com'
            }, {
                'id': 2,
                'address': 'foo2@bar.com'}
        ]
    }


def test_get_nonexisting_contact_results_in_404(test_app, client):
    assert client.get('/contacts?username=nonexisting1234').status_code == 404
    

def test_create_contact(test_app, dbpath, client):
    response = client.post('/contacts', json={
        'username': 'test_username',
        'firstname': 'test_firstname',
        'lastname': 'test_lastname'
    })

    assert response.status_code == 201
    assert len(ContactsSessionMaker(dbpath).make().query(Contact).all()) == 3
    

def test_create_contact_with_emails(test_app, dbpath, client):
    response = client.post('/contacts', json={
        'username': 'test_username',
        'firstname': 'test_firstname',
        'lastname': 'test_lastname',
        'emails': [
            {'address': 'e1@test.com'},
            {'address': 'e2@test.com'}
        ]
    })

    assert response.status_code == 201
    assert len(ContactsSessionMaker(dbpath).make().query(Contact).all()) == 3
    
    
def test_cannot_create_contact_with_missing_attributes(client):
    assert client.post('/contacts', json={
        'firstname': 'foo',
        'lastname': 'bar'
    }).status_code == 422

    assert client.post('/contacts', json={
        'username': 'foo',
        'lastname': 'bar'
    }).status_code == 422

    assert client.post('/contacts', json={
        'username': 'bar',
        'firstname': 'foo',
    }).status_code == 422
    
    
def test_update_contact(test_app, client):
    response = client.put('/contacts/1', json={
        'firstname': 'Newname'
    })

    assert response.status_code == 200
    assert json.loads(response.data.decode('utf-8'))['firstname'] == 'Newname'


def test_delete_contact(test_app, dbpath, client):
    response = client.delete('/contacts/1')
    assert response.status_code == 200
    assert len(ContactsSessionMaker(dbpath).make().query(Contact).all()) == 1    


def test_all_contacts_older_than_datetime(test_app, dbpath, client):
    response = client.delete('/contacts?until=2019-11-13T00:00:00+00:00')
    assert response.status_code == 200
    assert len(ContactsSessionMaker(dbpath).make().query(Contact).all()) == 1


def test_all_contacts_older_than_datetime_fails_with_invalid_param(client):
    response = client.delete('/contacts?until=123abc')
    assert response.status_code == 422

    # Requests with 'until' param result in response with status code 400
    response_2 = client.delete('/contacts')
    assert response_2.status_code == 400
    
    
def test_fails_to_delete_nonexistent_contact(client):
    assert client.delete('/contacts/200').status_code == 404


def test_add_contact_email(test_app, dbpath, client):
    response = client.put('/contacts/1/emails', json={'address': 'test3@abc.org'})
    assert response.status_code == 200
    assert len(ContactsSessionMaker(dbpath).make().query(Email).all()) == 4


def test_should_raise_error_if_email_is_invalid(client):
    assert client.put('/contacts/1/emails',
                      json={'address': 'invalid_address'}).status_code == 422
    
    
def test_remove_contact_email(test_app, dbpath, client):
    response = client.delete('/contacts/emails/1')
    assert response.status_code == 200
    assert len(ContactsSessionMaker(dbpath).make().query(Email).all()) == 2
    
