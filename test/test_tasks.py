from contactsapp.tasks import generate_contact 


def test_random_contact_info_is_generated():
    contact_info = generate_contact()

    for attr in ['username', 'firstname', 'lastname']:
        assert contact_info[attr] is not None

    # Check that the contact has some email addresses
    assert len(contact_info['emails']) > 0
