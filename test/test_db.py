from contactsapp.db import Contact, Email, ContactsSessionMaker
from sqlalchemy.exc import IntegrityError
import pytest


def test_session_maker_makes_sqlite_engine_session():
    session_maker = ContactsSessionMaker()
    session = session_maker.make()
    assert 'sqlite' == session.get_bind().engine.name

    # The database is in-memory by default
    assert 'sqlite:///:memory:' == str(session.get_bind().url)

    # An sqlite database file path can be passed as constructor argument
    file_session_maker = ContactsSessionMaker(path='session_test.db')
    assert 'sqlite:///session_test.db' in str(file_session_maker.make().get_bind().url)
    
  
def test_contact_contains_all_attributes():
    contact = Contact(username="johndoe123", firstname="John", lastname="Doe")
    assert contact.username == "johndoe123"
    assert contact.firstname == "John"
    assert contact.lastname == "Doe"
    
    # The mapped Contact object has an 'id' property, but it's None
    assert contact.id is None


def test_contact_can_have_multiple_email_addresses():
    email_1 = Email(address="johndoe123@gmail.com")
    email_2 = Email(address="johndoe123@yahoo.com")
    
    contact = Contact(username="johndoe123",
                      firstname="John",
                      lastname="Doe",
                      emails=[email_1, email_2])
    
    assert len(contact.emails) == 2


def test_emails_are_validated():
    with pytest.raises(AssertionError):
        Email(address="invalid_address")

    
def test_session_can_add_and_query_objects(app, dbpath):
    with app.app_context():
        session = ContactsSessionMaker(dbpath).make()
        assert len(session.query(Contact).all()) == 0

        email_1 = Email(address="foo@bar.com")
        email_2 = Email(address="foo2@bar.com")
        contact1 = Contact(username="foo",
                           firstname="foo_firstname",
                           lastname="foo_lastname",
                           emails=[email_1, email_2])

        email_3 = Email(address="bar@baz.com")
        contact2 = Contact(username="bar",
                           firstname="bar_firstname",
                           lastname="bar_lastname",
                           emails=[email_3])

        session.add_all([contact1, contact2])
        session.commit()
        
        assert len(session.query(Contact).all()) == 2

        contact = session.query(Contact).filter(Contact.username == "foo").one()
        assert contact is not None
        assert contact.created_at is not None

        
def test_contact_username_is_unique(app, dbpath):
    with app.app_context():
        session = ContactsSessionMaker(dbpath).make()
        session.add(Contact(username="foo",
                            firstname="foo_name",
                            lastname="foo_last"))
        session.commit()

        with pytest.raises(IntegrityError):
            session.add(Contact(username="foo",
                                firstname="bar",
                                lastname="baz"))
            session.commit()


def test_contact_fields_cannot_be_null(app, dbpath):
    with app.app_context():
        session = ContactsSessionMaker(dbpath).make()

        with pytest.raises(IntegrityError):
            session.add(Contact(username=None,
                                firstname="foo",
                                lastname="foo"))
            session.commit()
        session.rollback()
        
        with pytest.raises(IntegrityError):
            session.add(Contact(username="foo",
                                firstname=None,
                                lastname="foo"))
            session.commit()
        session.rollback()
            
        with pytest.raises(IntegrityError):
            session.add(Contact(username="foo",
                                firstname="foo",
                                lastname=None))
            session.commit()
        
